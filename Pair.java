public class Pair {
	private final int row;
	private final int column;

	public Pair(int row, int column) {
		this.row = row;
		this.column = column;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}

	@Override
	public int hashCode() {
		if (row < column) {
			return row ^ (column << 1);
		} else {
			return column ^ (row << 1);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		if ((((Pair) obj).getRow() == this.getRow() && ((Pair) obj).getColumn() == this
				.getColumn())
				|| (((Pair) obj).getRow() == this.getColumn() && ((Pair) obj)
						.getColumn() == this.getRow())) {
			return true;
		}
		return false;
	}

}
