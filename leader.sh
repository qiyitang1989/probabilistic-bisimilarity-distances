# create result folder to store the loggings
mkdir Result-Leader

# compile the program
javac -cp /eecs/home/qiyitang/experiment/BFSDistanceOne/commons-math3-3.4.1.jar Bisimilarity.java Pair.java SplayTree.java  Simple.java General.java MeasureTime.java 

# measure time
java -Xmx13g -cp .:/eecs/home/qiyitang/experiment/BFSDistanceOne/commons-math3-3.4.1.jar MeasureTime ../leader/leader3-4 0.0000001 1 ./Result-Leader/leader3_4-simple ./Result-Leader/leader3_4-general > Result-Leader/leader3-4-print
