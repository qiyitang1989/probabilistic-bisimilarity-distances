import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

public class ParseFile {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Use java ParseFile <inputFile>");
		} else {
			// process the command line arguments
			Scanner input = null;
			try {
				input = new Scanner(new File(args[0]));
			} catch (FileNotFoundException e) {
				System.out.printf("Input file %s not found%n", args[0]);
				System.exit(1);
			}

			PrintStream output = null;
			;
			try {
				output = new PrintStream(new File(args[0] + "_parsed"));
			} catch (FileNotFoundException e) {
				System.out.printf("Output file %s not created%n", args[1]);
				System.exit(1);
			}
			int i = 0;
			long[] bis = new long[8];
			long[] d1 = new long[8];
			int xth = 0;
			int yth = 0;
			while (input.hasNextLine()) {
				String str = input.nextLine();
				if (str.contains("time")) {
					i++;
					if (i <= 4) {
						continue;
					}
					if (str.contains("bisimilar")) {
						bis[xth++] = Long.parseLong(str.replace("bisimilar time = ", ""));
					} else {
						d1[yth++] = Long.parseLong(str.replace("distance one time = ", ""));

					}

				}

			}

			for (i = 0; i < 8; i++) {
				output.println(bis[i]);
			}
			output.println();
			for (i = 0; i < 8; i++) {
				output.println(d1[i]);
			}
			input.close();
			output.close();
		}
	}
}
