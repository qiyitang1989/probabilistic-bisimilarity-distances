import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;
import java.util.Random;
class Block {
	LinkedList<State> mList;
	// boolean isSplitter;
	SplayTree sTree;

	Block() {
		mList = new LinkedList<State>();
		// isSplitter = false;
		sTree = new SplayTree();
	}
}

class State {
	Block block;
	double sum;
	int idx;

	State(int index) {
		block = new Block();
		sum = 0;
		idx = index;
	}
}

public class Bisimilarity {
	public void lump(LinkedList<Block> partition, double[][] tranx) {
		LinkedList<Block> listOfSP = new LinkedList<Block>();
		for (Block b : partition) {
			// b.isSplitter = true;
			Block sp = new Block();
			for (State st : b.mList) {
				sp.mList.add(st);
			}
			listOfSP.add(sp);
		}
		while (!listOfSP.isEmpty()) {

			Block splitter = listOfSP.removeFirst();
			// System.out.println("SPLIT");
			if (splitter.mList.size() > 0) {
				split(splitter, partition, listOfSP, tranx);

			}

		}
	}

	public void split(Block sp, LinkedList<Block> partition, LinkedList<Block> listOfSP, double[][] tranx) {
		LinkedList<State> hasTrans = new LinkedList<State>();
		LinkedList<Block> hasSplit = new LinkedList<Block>();
		// initialize sum
		// System.out.print("splitters: ");
		// for (State s : sp.mList) {
		// System.out.print(s.idx + " ");
		// }
		// System.out.println();
		//
		// for (Block bb : partition) {
		// System.out.print("partition: ");
		// for (State s : bb.mList) {
		// System.out.print(s.idx + " ");
		// }
		// }
		// System.out.println();

		for (State sj : sp.mList) {
			for (Block b : partition) {
				for (State si : b.mList) {
					if (tranx[si.idx][sj.idx] > 0) {
						si.sum = 0;
						if (!hasTrans.contains(si))
							hasTrans.add(si);
					}
				}
			}
		}
		// update sum to sp
		for (State sj : sp.mList) {
			for (State si : hasTrans) {
				si.sum += tranx[si.idx][sj.idx];
			}
		}

		// split splay tree
		for (State si : hasTrans) {
			Block b = si.block;

			// System.out.print("hasTrans: ");
			// for (State s : b.mList) {
			// System.out.print(s.idx + " ");
			// }
			// System.out.println();

			b.mList.remove(si);
			b.sTree.insert(si.sum, si);
			if (!hasSplit.contains(b)) {
				hasSplit.add(b);
			}
		}
		// System.out.println(hasSplit.size());
		for (Block b : hasSplit) {
			// System.out.print("hasSplit: ");
			// for (State s : b.mList) {
			// System.out.print(s.idx + " ");
			// }
			// System.out.println("b.isSplitter" + b.isSplitter);
			if (b.mList.size() == 0) {
				partition.remove(b);
			}
			//LinkedList<Block> tempList = new LinkedList<Block>();
			LinkedList<BinaryNode> queue = new LinkedList<BinaryNode>();

			// Block newSp = new Block();
			// for (State st : b.mList) {
			// newSp.mList.add(st);
			// }
			// if (newSp.mList.size() > 0)
			// listOfSP.add(newSp);
			int max = -1;
			Block maxBlk = null;

			queue.add(b.sTree.getRoot());
			while (!queue.isEmpty()) {

				BinaryNode temp = queue.removeFirst();
				// temp.b.isSplitter = false;

				partition.add(temp.block);

				Block newSp = new Block();
				for (State st : temp.block.mList) {
					newSp.mList.add(st);
				}
				listOfSP.add(newSp);
				if (max == -1) {
					max = newSp.mList.size();
					maxBlk = newSp;
				} else if (newSp.mList.size() > max) {
					max = newSp.mList.size();
					maxBlk = newSp;
				}

				// System.out.print("tempList: ");
				// for (State s : temp.block.mList) {
				// System.out.print(s.idx + " ");
				// }
				// System.out.println();
				// System.out.println(temp.b.ls.size());
				if (temp.left != null) {
					queue.add(temp.left);
				}
				if (temp.right != null) {
					queue.add(temp.right);
				}
			}
			b.sTree = new SplayTree();
			if (b.mList.size() != 0) {
				Block newSp = new Block();
				for (State st : b.mList) {
					newSp.mList.add(st);
				}
				listOfSP.add(newSp);
				if (newSp.mList.size() > max) {
					max = newSp.mList.size();
					maxBlk = newSp;
				}
			}
			listOfSP.remove(maxBlk);

			// // if (!b.isSplitter) {
			// if (b.mList.size() > 0)
			// tempList.add(b);
			//
			// // int max = -1;
			// // Block maxBlk = null;
			// for (Block bb : tempList) {
			// // bb.isSplitter = true;
			// if (max == -1) {
			// max = bb.mList.size();
			// maxBlk = bb;
			// } else if (bb.mList.size() > max) {
			// max = bb.mList.size();
			// maxBlk = bb;
			// }
			// }
			// // System.out.print("maxBlk: ");
			// // for (State s : maxBlk.mList) {
			// // System.out.print(s.idx + " ");
			// // }
			// // System.out.println();
			// tempList.remove(maxBlk);
			// // maxBlk.isSplitter = false;
			// // }
			//
			// // if ((b.isSplitter && tempList.size() >= 1) || !b.isSplitter) {
			// for (Block block : tempList) {
			//
			// // the newly added splitters
			// // System.out.print("New SPlitter: ");
			// Block newSp = new Block();
			// for (State st : block.mList) {
			// newSp.mList.add(st);
			// // System.out.print(st.idx + " ");
			// }
			// // System.out.println();
			//
			// // newSp.isSplitter = true;
			// //listOfSP.add(newSp);
			//
			// print the list splitters
			// for (Block bb : listOfSP) {
			// System.out.print("listOfSP: ");
			// for (State s : bb.mList) {
			// System.out.print(s.idx + " ");
			// }
			// }
			// System.out.println();

			// }
			// }

		}

	}

	public static void main(String[] args) {
		// initialize
		if (args.length != 2) {
			System.out.println("Use java Bisimilarity " + "<inputFile>" + "<outputfile>");
			return;
		} else {
			// process the command line arguments
			Scanner input = null;
			try {
				input = new Scanner(new File(args[0]));
			} catch (FileNotFoundException e) {
				System.out.printf("Input file %s not found%n", args[0]);
				System.exit(1);
			}

			PrintStream output = null;
			;
			try {
				output = new PrintStream(new File(args[1]));
			} catch (FileNotFoundException e) {
				System.out.printf("Output file %s not created%n", args[1]);
				System.exit(1);
			}

			// process the input file
			int numOfStates = -1;
			double[] labels = null;
			double[][] tranFunc = null;

			while (input.hasNextInt()) {
				try {
					numOfStates = input.nextInt();
					System.out.println(numOfStates);
					int numOfTrans = input.nextInt();
					
					labels = new double[numOfStates];
					for (int i = 0; i < numOfStates; i++) {
						//labels[i] = input.nextDouble();
						labels[i] = new Random().nextInt(2);
						assert labels[i] >= 0 : String.format("Label %f of state %d is smaller than 0", labels[i], i);
						assert labels[i] <= 1 : String.format("Label %f of state %d is greater than 1", labels[i], i);
					}

					tranFunc = new double[numOfStates][numOfStates];

//					double error = 0;
//					for (int i = 0; i < numOfStates; i++) {
//						double row = 0;
//						for (int j = 0; j < numOfStates; j++) {
//							tranFunc[i][j] = input.nextDouble();
//							row += tranFunc[i][j];
//							error += Math.ulp(row);
//						}
//						assert Math.abs(row - 1) < error : String.format("Row of state %d adds up to %f", i, row);
//					}
					for(int i = 0; i < numOfTrans; i++ ){
						int row = input.nextInt();
						int col = input.nextInt();
						System.out.println(row);
						System.out.println(col);
						
						tranFunc[row][col] = input.nextDouble();
						System.out.println(tranFunc[row][col]);
					}		
					// input.nextLine();
				} catch (NoSuchElementException e) {
					System.out.printf("Input file %s not in the correct format%n", args[0]);
				}

				//long start = System.nanoTime();
				// bisimilarity test
				HashMap<Double, Block> hm = new HashMap<Double, Block>();
				for (int i = 0; i < numOfStates; i++) {
					if (hm.containsKey(labels[i])) {
						State s = new State(i);
						s.block = hm.get(labels[i]);
						hm.get(labels[i]).mList.add(s);
					} else {
						Block b = new Block();
						State s = new State(i);
						s.block = b;
						b.mList.add(s);
						hm.put(labels[i], b);
					}
				}

				LinkedList<Block> partition = new LinkedList<Block>();
				partition.addAll(hm.values());

				Bisimilarity bis = new Bisimilarity();
				bis.lump(partition, tranFunc);

				// results
				System.out.println("Partition: ");
				for (Block b : partition) {
					 System.out.print(b.mList.size());
					 if (b.mList.size() > 0)
					 System.out.println(" " + b.mList.getLast().idx);
				}

				// write partitions to file
				for (Block b : partition) {
					for (State ss : b.mList) {
						output.print(ss.idx + " ");
					}
					output.print("   ");
				}
				output.println();

			}

		}

	}
}
